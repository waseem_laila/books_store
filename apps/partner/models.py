from django.db import models
from oscar.apps.partner.abstract_models import AbstractPartner


class Partner(AbstractPartner):

    # the field I want to add:
    company = models.CharField(max_length=200, null=True)


# I need to make this import last thing.
from oscar.apps.partner.models import *
